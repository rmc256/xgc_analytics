#

#main level script to read analysis data file, and call analysis functions
import f0analysis as f0ana
import numpy as np

import argparse
import sys, traceback
import time
import os

def dumpvars(f):
	print 'File: ', f.name
	for v in f.vars.keys():
		print '%15s: %r' % (v, np.average(f[v][...]))

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', default='xgc.dataspaces.f0analysis.bp', help="filename (default=xgc.dataspaces.f0analysis.bp)")
parser.add_argument('-o', '--output', help="output filename")
parser.add_argument('--iphi', help="phi index", type=int)
parser.add_argument('--subset', help="subset (e.g. 'range(1,10)')")
parser.add_argument('--timeout', help="timeout seconds", type=float)
parser.add_argument('--nostream', help="no stream", action='store_true')
parser.add_argument('--nompi', help="no MPI use", action='store_true')
parser.add_argument('method', nargs='?', default='BP', help="Adios read method (default=BP)")
parser.add_argument('params', nargs='?', default='verbose=3', help="method parameters (default=verbose=3)")
args = parser.parse_args()

f0filename = args.input
method = args.method
params = args.params
timeout = 300.0 if args.timeout is None else args.timeout
iphi = None if args.iphi is None else args.iphi
subset = None if args.subset is None else eval(args.subset)
is_stream = True if args.nostream is None else not args.nostream
is_usempi = True if args.nompi is None else not args.nompi

if not is_stream:
	timeout = 0.0

## MPI
if is_usempi:
	from mpi4py import MPI
	import adios_mpi as ad

	comm_size = MPI.COMM_WORLD.Get_size()
	comm_rank = MPI.COMM_WORLD.Get_rank()
else:
	import adios as ad
	comm_size = 1
	comm_rank = 0

## ADIOS read init
ad.read_init(method, parameters=params)

###### read data using ADIOS ##################################################
f = ad.file(f0filename, method, is_stream=is_stream, timeout_sec=-1, lock_mode=1)

## static variables
fm = ad.file('xgc.dataspaces.f0analysis.mesh.bp', method, is_stream=is_stream, timeout_sec=-1, lock_mode=1)
start_time = time.time()
#Read in RZ, and psin
psi = fm['psi'][...] #this is grid%psi in XGC1
psi_x = fm['eq_x_psi'][...]
psin = psi / psi_x
RZ = fm['/coordinates/values'][...] #this is grid%x in XGC1

#dumpvars(fm)
#dumpvars(f)

#Read in f0 mesh variables
class structtype():
	def copy(self):
		new = structtype()
		new.__dict__  = self.__dict__.copy()
		return new
	def __getitem__(self,inds):
		new = self.copy()
		new.f0_T_ev = new.f0_T_ev[:,inds]
		new.f0_den = new.f0_den[inds]
		new.f0_grid_vol_vonly = new.f0_grid_vol_vonly[:,inds]
		return new

meshvars = ['f0_nvp','f0_nmu','f0_vp_max','f0_smu_max',
			'f0_grid_vol_vonly','f0_den','f0_T_ev',
			'ptl_mass','ptl_charge']
mesh = structtype()
for v in meshvars:
	setattr(mesh,v,fm[v][...])
fm.close()

## dynamic variables
#f = ad.file(f0filename) #jyc: move to the top
nphi = int(f['nphi'][...])

if iphi is not None:
	mypiece = slice(iphi, iphi+1)
else:
	mypiece = slice((nphi/comm_size)*comm_rank, (nphi/comm_size)*(comm_rank+1))
nphi_off = mypiece.start

istep = 1
while True:
	try:
		if comm_rank==0: print ">>> step:", istep

		f0_fXGC = f['f0_f'][mypiece,:]
		print "comm_rank, mypiece = ", comm_rank, mypiece, f0_fXGC.shape
		nnode = int(f['nnode'][...])
		#TODO Decide on a final form, for now reshape for what f0analysis assumes
		#XGC1 writes out in [Nplanes,Nsp,Nvpe,Ngrid,Nvpa]
		#f0analysis assumes [Nvpa,Nplanes,Ngrid,Nvpe,Nsp]
		f0_f = np.transpose(f0_fXGC,axes=(4,0,3,2,1))
		sml_gstep = int(f['sml_gstep'][...])
		#f.close()

		###### calculate moments ##################################################
		#ions
		ni3d,ViPar3d,Ti3d,TiPar3d,TiPerp3d = f0ana.f0_moments(mesh,f0_f,iselectrons=False)
		#electrons
		ne3d,VePar3d,Te3d,TePar3d,TePerp3d = f0ana.f0_moments(mesh,f0_f,iselectrons=True)


		##### f0_f contour viz ####################################################
		tol = 1e-4
		psin_min = 0.9
		psin_max = 1.05
		#cutout region 3 (private flux) and wall nodes (at end of array)
		solInd = np.where(psin > 1.01)[0][0]
		psinInInds = np.where(psin < 0.995)[0]
		rgn3StartInd = psinInInds[psinInInds > solInd]
		if rgn3StartInd.size == 0:
			psin12 = psin
		else:
			psin12 = psin[0:rgn3StartInd[0]]
		#get flux surface psin
		#sort first (some like cyclone base case have last flux surface with odd psin interleaved)
		psin12sort = psin12.copy()
		psin12sort.sort()
		psin_surf = psin12sort[np.diff(psin12sort) > tol]

		#create poloidal plane indexes (inds) of plots to create
		indsSurf = np.where( (psin_surf > psin_min) & (psin_surf < psin_max) )[0]
		inds = []
		Nsurf = 5
		if indsSurf.size > Nsurf:
			indsSurf = indsSurf[::indsSurf.size/Nsurf]
		Npersurf = 4 #plots to make per surface
		for ind in indsSurf:
			indsTmp = np.where(np.abs(psin12 - psin_surf[ind]) < tol)[0]
			inds += indsTmp[2:-2:indsTmp.size/Npersurf].tolist()

		## plot normal contours
		meshviz = mesh[inds]
		f0_fviz = f0_f[:,:,inds,:,:]
		fileDir = 'f0analysis.%05d' % istep
		try:
			os.mkdir(fileDir)
		except:
			pass
		#ions
		f0ana.plot_f0_contours(meshviz, f0_fviz[...,0], sml_gstep, RZ=RZ, \
								fileDir=fileDir+'/', title='xgc.i_f.viz', \
								poffset=nphi_off, inds=subset)
		#electrons
		f0ana.plot_f0_contours(meshviz, f0_fviz[...,1], sml_gstep, RZ=RZ, \
								fileDir=fileDir+'/', title='xgc.e_f.viz', \
								poffset=nphi_off, inds=subset)

		## plot non-Maxwellian contours
		#ions
		f0ana.plot_f0non_contours(meshviz, f0_fviz, sml_gstep, iselectrons=False, RZ=RZ, \
								fileDir=fileDir+'/', title='xgc.i_fnon.viz', \
								poffset=nphi_off, inds=subset)
		#electrons
		f0ana.plot_f0non_contours(meshviz, f0_fviz, sml_gstep, iselectrons=True, RZ=RZ, \
								fileDir=fileDir+'/', title='xgc.e_fnon.viz', \
								poffset=nphi_off, inds=subset)
	except (KeyboardInterrupt, SystemExit):
		raise
	except Exception as e:
		traceback.print_exc()
		pass


	###### write data to file #################################################
	ad.init_noxml()
	fname = 'xgc.f0analysis.%05d.bp' % istep if args.output is None else args.output
	if comm_rank==0: print 'Writing ...', fname
	fw = ad.writer(fname)
        if is_usempi:
                fw.declare_group('group', method='MPI')
        else:
                fw.declare_group('group', method='POSIX1')
	## Shape
	## f0_f: [Nvpa,Nplanes,Ngrid,Nvpe,Nsp]
	## Others: [Nplanes,Ngrid]
	f0_f_lshape = f0_f[...,0].shape
	f0_f_gshape = (f0_f_lshape[0], nphi, f0_f_lshape[2], f0_f_lshape[3])
	f0_f_offset = (0, nphi_off, 0, 0)

	v_lshape = ViPar3d.shape
	v_gshape = (nphi, nnode)
	v_offset = (nphi_off, 0)

	## Define local, global dims and offsets for parallel writing
	fw.define_var('i_f', f0_f_lshape, f0_f_gshape, f0_f_offset)
	fw.define_var('e_f', f0_f_lshape, f0_f_gshape, f0_f_offset)
	fw.define_var('ni3d', v_lshape, v_gshape, v_offset)
	fw.define_var('ViPar3d', v_lshape, v_gshape, v_offset)
	fw.define_var('Ti3d', v_lshape, v_gshape, v_offset)
	fw.define_var('TiPar3d', v_lshape, v_gshape, v_offset)
	fw.define_var('TiPerp3d', v_lshape, v_gshape, v_offset)
	fw.define_var('ne3d', v_lshape, v_gshape, v_offset)
	fw.define_var('VePar3d', v_lshape, v_gshape, v_offset)
	fw.define_var('Te3d', v_lshape, v_gshape, v_offset)
	fw.define_var('TePar3d', v_lshape, v_gshape, v_offset)
	fw.define_var('TePerp3d', v_lshape, v_gshape, v_offset)

	Nmoms = 10 #number of moments calculated
	fw['Nmoms'] = Nmoms

	fw['i_f'].value = f0_f[...,0]
	fw['e_f'].value = f0_f[...,1]
	fw['ni3d'].value = ni3d
	fw['ViPar3d'].value = ViPar3d
	fw['Ti3d'].value = Ti3d
	fw['TiPar3d'].value = TiPar3d
	fw['TiPerp3d'].value = TiPerp3d
	fw['ne3d'].value = ne3d
	fw['VePar3d'].value = VePar3d
	fw['Te3d'].value = Te3d
	fw['TePar3d'].value = TePar3d
	fw['TePerp3d'].value = TePerp3d
	fw.close()

	end_time = time.time()
	print "[%d] Done. Elapsed (sec): %.3f" % (comm_rank, end_time - start_time)

	if comm_rank==0: print ">>> waiting a next step ..."
	if (f.advance(timeout_sec=timeout) < 0):
		break

	istep += 1

if comm_rank==0: print "[%d] Done."
f.close()
ad.finalize()
