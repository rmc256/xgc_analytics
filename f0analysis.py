#/usr/bin python
import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.pyplot as plt

######## ANALYSIS ###################################################################
def f0_moments(mesh, f0_f, iselectrons=True):
	"""Calculate moments of the f0 distribution function.

	Inputs:
	mesh
		f0_nvp: # v// grid points [1]
		f0_nmu: # vperp grid points [1]
		f0_vp_max: Maximum v// / vth value [1]
		f0_smu_max: Maximum vperp / vth value [1]
		f0_grid_vol_vonly: Velocity space grid volume (used for integrals) [Ngrid,2]
		f0_T_ev: Initial temperature [Ngrid,2]
		ptl_mass: Mass of each species [2]
		ptl_charge: Charge of each species [2]
	f0_f: Distribution function [Nvpa, Nplanes,Ngrid, Nvpe,2] (Nplanes is not present for XGCa,
			handles both cases using ... in einsum)
	iselectrons: True if for electron species (isp==1), False for ions (isp==0)

	Outputs:
	den3d: Density (0th order moment) [Ngrid]
	Vpar3d: Parallel velocity (1st order moment) [Ngrid]
	T3d: Temperature (2nd order moment) [Ngrid]
	Tpar3d: Parallel temperature
	Tperp3d: Perpendicular temperature
	"""

	isp = int(iselectrons)
	# ; Create vp and mu grid
	vpa, vpe, vpe1 = create_vpa_vpe_grid(mesh.f0_nvp, mesh.f0_nmu, mesh.f0_vp_max, mesh.f0_smu_max)
	#discrete cell correction
	volfac = np.ones((vpa.size,vpe.size))
	volfac[:,0]=0.5 #0.5 for where ivpe==0

	# Extract species of interest (0 ions, 1 electrons)
	mass = mesh.ptl_mass[isp]
	charge = mesh.ptl_charge[isp]

	vspace_vol = mesh.f0_grid_vol_vonly[isp,:]
	Tev = mesh.f0_T_ev[isp,:]
	f0 = f0_f[...,isp]

	#multiply f0 with dv (vspace_vol)
	tmp = np.einsum('i...k,ik->i...k',f0,volfac)
	tmp = np.einsum('i...k,...->i...k',tmp,vspace_vol)

	vth=np.sqrt(np.abs(charge)*Tev/mass)

	#calculate moments of f0 using einsum for fast(er) calculation
	den3d = np.einsum('i...k->...',tmp)
	Vpar3d = np.einsum('i...k,i->...',tmp,vpa)/den3d
	vpa1 = np.add.outer(vpa,-Vpar3d) #vpa - Vpar3d, [Nvpa,Ngrid]
	Vpar3d = np.einsum('...i,i->...i',Vpar3d,vth)

	prefac = mass*vth**2./(2.*np.abs(charge))
	vsqr = np.add.outer(vpa1**2.,vpe**2.) #vsqr = (vpa-Vpar3d)**2 + vpe**2, [Nvpa,Ngrid,Nvpe]
	T3d = 2./3.*prefac*np.einsum('i...k,i...k->...',tmp,vsqr)/den3d

	Tpar3d = 2.*prefac*np.einsum('i...k,i->...',tmp,vpa**2.)/den3d
	Tperp3d = prefac*np.einsum('i...k,k->...',tmp,vpe**2.)/den3d

	#TODO: Add calculation for fluxes, Vpol (requires more info)

	return (den3d,Vpar3d,T3d,Tpar3d,Tperp3d)


def create_vpa_vpe_grid(f0_nvp, f0_nmu, f0_vp_max, f0_smu_max):
	"""Create velocity grid vectors"""
	vpe=np.linspace(0,f0_smu_max,f0_nmu+1) #dindgen(nvpe+1)/(nvpe)*vpemax
	vpe1=vpe.copy()
	vpe1[0]=vpe[1]/3.
	vpa=np.linspace(-f0_vp_max,f0_vp_max,2*f0_nvp+1)
	return (vpa, vpe, vpe1)


def f_nonMaxwellian(mesh, f0_f, initial=False, iselectrons=True):

	#chose initial den,temp profiles or distribution moments profiles
	isp = int(iselectrons)
	f0 = f0_f[...,isp]

	if initial:
		den = mesh.f0_den
		temp = mesh.f0_T_ev[isp,:]
		Vpar = 0.*den
	else: #default
		den,Vpar,temp,_,_ = f0_moments(mesh,f0_f,iselectrons=iselectrons)


	fMaxwellian = create_Maxwellian(mesh,den,Vpar,temp,iselectrons=iselectrons)

	fNonMaxwellian = f0 - fMaxwellian

	return fNonMaxwellian,fMaxwellian


def create_Maxwellian(mesh,den,Vpar,temp,dpot=None,iselectrons=True):

	isp = int(iselectrons)
	charge = mesh.ptl_charge[isp]
	mass = mesh.ptl_mass[isp]

	# ; Create vp and mu grid
	vpa, vpe, vpe1 = create_vpa_vpe_grid(mesh.f0_nvp, mesh.f0_nmu, mesh.f0_vp_max, mesh.f0_smu_max)

	f0temp = mesh.f0_T_ev[isp,:]
	if Vpar.ndim > 1:
		f0temp = np.tile(f0temp,(Vpar.shape[0],1))

	nOverT = den/temp
	VparOverVth = Vpar/np.sqrt(np.abs(charge)*f0temp/mass)
	f0tempOvertemp = f0temp/temp
	if not dpot is None:
		dpotOverT = charge/abs(charge)*dpot/temp
	else:
		dpotOverT = np.zeros(temp.shape)

	fMaxwellian=np.empty(vpa.shape+den.shape+vpe.shape)

	for (ivpa,vpai) in enumerate(vpa):
		for (ivpe,vpei) in enumerate(vpe):
			eps = ((vpai-VparOverVth)**2.+vpei**2.)/2.

			fMaxwellian[ivpa,...,ivpe] = np.sqrt(f0tempOvertemp)*nOverT * np.exp(-f0tempOvertemp*eps - dpotOverT)*vpe1[ivpe]

	return fMaxwellian

def f_parallel(mesh,f0_f,iselectrons=True):
	'''Creates a 1d distribution function, summing over the perpendicular direction'''
	
	isp = int(iselectrons)
	# ; Create vp and mu grid
	vpa, vpe, vpe1 = create_vpa_vpe_grid(mesh.f0_nvp, mesh.f0_nmu, mesh.f0_vp_max, mesh.f0_smu_max)
	#discrete cell correction
	volfac = np.ones((vpa.size,vpe.size))
	volfac[:,0]=0.5 #0.5 for where ivpe==0

	# Extract species of interest (0 ions, 1 electrons)
	mass = mesh.ptl_mass[isp]
	charge = mesh.ptl_charge[isp]

	Tev = mesh.f0_T_ev[isp,:]
	vspace_vol = mesh.f0_grid_vol_vonly[isp,:]
	
	# from XGCa, f0_grid_vol_vonly(i,isp)=T_ev*sqrt(1D0/sml_2pi)*f0_dsmu*f0_dvp
	#this means that to do only the vperp integral, we need to divide this by f0_dvp
	vspace_vol = vspace_vol/np.mean(np.diff(vpa))
	
	#multiply f0 with dv (vspace_vol)
	tmp = np.einsum('i...k,ik->i...k',f0_f,volfac)
	tmp = np.einsum('i...k,...->i...k',tmp,vspace_vol)
	
	vth=np.sqrt(np.abs(charge)*Tev/mass)

	#calculate moments of f0 using einsum for fast(er) calculation
	fparallel = np.einsum('...k->...',tmp)

	return fparallel
 
############# VIZ ###########################################
def f0_contour(vpa, vpe, f, levels = None, log = True):
	"""plot distribution functions at a single point

	Inputs
		vpa 	Normalized parallel velocity [Nvpa]
		vpe 	Normalized perpendicular velocity [Nvpe]
		f 		Distribution function [Nvpa, Nvpe]
		levels	Levels input to contour
		log 	True to plot the log of the data
	Outputs
		fig 	Handle to figure
	"""
	fig = plt.figure(0)
	fig.clf()
	if log:
		f = np.log10(f)
	if (levels is None) & (log):
		bot = np.ceil(np.nanmin(f))
		if bot < 0: bot = 0.
		top = np.floor(np.nanmax(f))
		levels = np.linspace(bot,top,top-bot+1)
	if (levels is None) & (~log):
       		#indsneg = f<0
		#f = np.log10(np.abs(f))
		#f[indsneg] = -f[indsneg]
		bot = np.ceil(np.nanmin(f))
		top = np.floor(np.nanmax(f))
		levels = np.linspace(bot,top,20)

	plt.contour(vpa,vpe,f.T,levels=levels)
	plt.xlabel('$v_{//}/v_{th}$')
	plt.ylabel('$v_\perp/v_{th}$')
	plt.grid()
	plt.colorbar()
	return fig

def plot_f0_contours(mesh, f, step,
				RZ = None, inds = None,
				fileDir = '', title='xgc.f0viz',
				levels = None, log = True, poffset = 0):

	if inds is None:
		fshape = f.shape
		inds = np.arange(0,fshape[2])

	vpa, vpe, vpe1 = create_vpa_vpe_grid(mesh.f0_nvp, mesh.f0_nmu, mesh.f0_vp_max, mesh.f0_smu_max)

	planes = np.arange(0,f.shape[1]) #TODO: Allow variable planes?
	for i in inds:
		for p in planes:
			fig = f0_contour(vpa, vpe, f[:,p,i,:], levels = levels, log = log)
			if RZ is None:
				fname = fileDir+title+'.p'+str(p+poffset)+ \
					'.i'+str(i)+ \
					'.t'+str(step)+'.jpg'
				print 'Saving ... ', fname
				fig.savefig(fname)
			else:
				fname = fileDir+title+'.p'+str(p+poffset)+ \
					'.R'+str('%0.4f' % RZ[i,0])+'.Z'+str('%0.4f' % RZ[i,1])+ \
					'.t'+str(step)+'.jpg'
				print 'Saving ... ', fname
				fig.savefig(fname)

def plot_f0non_contours(mesh, f0_f, step, iselectrons=True, **kwargs):
	fnon, fmax = f_nonMaxwellian(mesh, f0_f, iselectrons=iselectrons)
	kwargs['log'] = False
 	if 'title' not in kwargs.keys():
		kwargs['title'] = 'xgc.f0nonviz'
	plot_f0_contours(mesh, fnon, step, **kwargs)
