#/usr/bin python

import f0analysis as f0ana
import numpy as np
import adios as ad


############ WARNING ############################
#NEED TO THINK ABOUT DOING THIS FOR TOTAL-F
#DO I FIRST NEED TO 





###### read data using ADIOS ##################################################
#this will be using Dataspaces or other
#read in latest timestep f0_f
#f0_f is size [Nvp,Ngrid,Nvmu,2]
# where Ngrid is the number of grid points in each local process

############### calculate non-Maxwellian distribution #########################
#fNon and fMax are size [Nvp,Ngrid,Nvmu], 
fNonMaxi,fMaxi = f0ana.f_nonMaxwellian(mesh, f0_f, initial=False, iselectrons=False)
fNonMaxe,fMaxe = f0ana.f_nonMaxwellian(mesh, f0_f, initial=False, iselectrons=True)


############### 
fact_sample = 10. #sampling factor for XGC1 -> XGCa simulation
sample_nonmax = []
sample_max = 1. - sample_nonmax